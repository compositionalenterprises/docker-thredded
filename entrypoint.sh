#!/bin/bash

bundle exec rake db:migrate
bundle exec rake db:seed
bin/rails server --port $"{THREDDED_PORT}" --binding 0.0.0.0
