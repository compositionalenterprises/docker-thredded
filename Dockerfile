FROM alpine:3
MAINTAINER Andrew Cziryak <andrewcz@andrewcz.com>

ARG THREDDED_PORT=8080
ENV THREDDED_PORT ${THREDDED_PORT}
ARG THREDDED_VERSION='0.16.14'
ENV THREDDED_VERSION ${THREDDED_VERSION}

ENV BUNDLE_SILENCE_ROOT_WARNING=1
ENV BUNDLE_PATH=/bundle
ENV DOCKER=1
ENV APP_HOME /thredded

ARG THREDDED_CONFIG_PATH=${APP_HOME}/config
ENV THREDDED_CONFIG_PATH ${THREDDED_CONFIG_PATH}
WORKDIR $APP_HOME

RUN apk add --no-cache \
    # Runtime deps
    ruby ruby-bundler ruby-bigdecimal ruby-io-console tzdata nodejs bash \
    # Bundle install deps
    build-base ruby-dev libc-dev libffi-dev linux-headers gmp-dev libressl-dev libxml2-dev libxslt-dev \
    mariadb-connector-c-dev postgresql-dev sqlite-dev git && \
    # Mkdir the pids dir for later
    mkdir -p $APP_HOME/tmp/pids

# Copy Gemfile and run bundle install first to allow for caching
COPY ./thredded/lib/thredded/version.rb $APP_HOME/lib/thredded/
COPY ./thredded/thredded.gemspec ./thredded/shared.gemfile ./thredded/i18n-tasks.gemfile ./thredded/rubocop.gemfile ./thredded/Gemfile ./entrypoint.sh $APP_HOME/
COPY ./thredded/.rubocop.yml ./thredded/.rspec ./thredded/Rakefile ./thredded/config.ru ./thredded/app ./thredded/bin ./thredded/config ./thredded/db ./thredded/lib ./thredded/script ./thredded/spec ./thredded/vendor $APP_HOME/


RUN bundle --path=$BUNDLE_PATH -j $(nproc)

EXPOSE ${THREDDED_PORT}
VOLUME ${THREDDED_CONFIG_PATH}

ENTRYPOINT ["./entrypoint.sh"]
